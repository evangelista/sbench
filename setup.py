#!/usr/bin/env python3

"""Setup script for sbench."""

import os
from setuptools import setup, find_packages

from sbench import mdata


classifiers = [
    'Programming Language :: Python',
    'Operating System :: OS Independent',
    'License :: OSI Approved :: GNU General Public License v3 (GPLv3)'
]


setup(
    name=mdata.NAME,
    version=mdata.VERSION,
    description=mdata.DESCRIPTION,
    long_description=open('README.md').read(),
    long_description_content_type='text/markdown',
    url=mdata.URL,
    packages=find_packages('.'),
    classifiers=classifiers,
    package_dir={'': '.'},
    scripts=[
        'scripts/sbench'
    ],
    package_data={
        'sbench': [
            os.path.join('data', '*')
        ]
    },
    install_requires=[
        'ply'
    ]
)
