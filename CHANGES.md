# Version NEXT

* implementation of action `show`


# Version 1.0.1

* added missing `ply` dependency in `setup.py`

* fixed 404 error in `README.md`


# Version 1.0.0

* first version