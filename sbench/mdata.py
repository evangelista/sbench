#!/usr/bin/env python3

"""Defines some meta data for sbench."""

NAME = 'sbench'

DESCRIPTION = 'simple benchmarking tool'

VERSION = '1.1.0'

URL = 'https://depot.lipn.univ-paris13.fr/evangelista/sbench'
