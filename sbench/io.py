#!/usr/bin/env python3

"""Module IO provides some methods to print stuff."""

import sys

VERBOSE: bool = False


def msg(message: str) -> None:
    """Print message to stdout if io.VERBOSE is True."""
    if VERBOSE:
        print(message)


def err(message: str) -> None:
    """Print an error message to stderr and exit with code 1."""
    print(f'error: {message}', file=sys.stderr)
    sys.exit(1)
