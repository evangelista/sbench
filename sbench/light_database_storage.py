#!/usr/bin/env python3

"""Definition of class LightDatabaseStorage."""

import typing as tp
import os
import datetime
import sqlite3
import pkg_resources

from . import Storage
if tp.TYPE_CHECKING:
    from . import Run, Benchmark


class LightDatabaseStorage(Storage):
    """A class to store run results in an sqlite3 database."""

    def __init__(self, bench: 'Benchmark', **kwargs: tp.Any):
        """Initialise self."""
        super().__init__(bench, **kwargs)
        db_file = self._get_db_file()
        self.conn: tp.Optional[sqlite3.Connection] = None
        if os.path.exists(db_file):
            self.conn = sqlite3.connect(db_file)

    def setup(self) -> None:
        super().setup()
        if self.conn is not None:
            return
        sql_file = pkg_resources.resource_filename(
            'sbench', os.path.join('data', 'init.sql')
        )
        self.conn = sqlite3.connect(self._get_db_file())
        with open(sql_file, encoding='utf-8') as fd:
            cur = self.conn.cursor()
            cur.executescript(fd.read())
        self.conn.commit()

    def _get_db_file(self) -> str:
        return os.path.join(self.bench.get_dir(), 'runs.db')

    def store(self, run: 'Run') -> None:
        assert self.conn is not None
        cur = self.conn.cursor()
        try:
            cur.execute(
                (
                    'insert into run(assign, command, exit_code, timeout, '
                    'start_time, end_time, exec_time, stdout, stderr, data) '
                    'values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                ), (
                    run.get_id(),
                    run.get_full_cmd(),
                    run.exit_code,
                    run.timeout,
                    run.start_time,
                    run.end_time,
                    run.exec_time,
                    run.stdout,
                    run.stderr,
                    run.data
                )
            )
        except sqlite3.IntegrityError:
            return
        self.conn.commit()

    def load(self, run: 'Run') -> None:
        if self.conn is None:
            return
        cur = self.conn.cursor()
        sql = (
            'select exit_code, timeout, start_time, end_time, exec_time, '
            'stdout, stderr, data '
            'from run where assign = ?'
        )
        sql_args = (run.get_id(), )
        row = cur.execute(sql, sql_args).fetchone()
        if row is not None:
            run.done = True
            run.exit_code = row[0]
            run.timeout = row[1]
            run.start_time = datetime.datetime.fromisoformat(row[2])
            run.end_time = datetime.datetime.fromisoformat(row[3])
            run.exec_time = row[4]
            run.stdout = row[5]
            run.stderr = row[6]
            run.data = bytes(row[7])

    def delete(self, run: 'Run') -> bool:
        if self.conn is None:
            return False
        sql = 'delete from run where assign = ?'
        sql_args = (run.get_id(), )
        cur = self.conn.cursor()
        cur.execute(sql, sql_args)
        result = cur.rowcount > 0
        self.conn.commit()
        return result

    def serialise(self) -> tp.Dict[str, tp.Any]:
        return {
            'type': 'lightdb'
        }
