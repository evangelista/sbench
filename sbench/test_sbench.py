#!/usr/bin/env python3

"""Test module for sbench."""

from . import Benchmark


def mk_test_ls_benchmark() -> Benchmark:
    """Return a test benchmark that executes ls with various parameters."""
    return Benchmark(**{
        "name": "test-ls",
        "cmd": "/usr/bin/ls -1",
        "timeout": 2,
        "params": {
            "long": {
                "long": "-l",
                "no-long": ""
            },
            "reverse": {
                "reverse": "-r",
                "no-reverse": ""
            },
            "all": {
                "all": "-a",
                "no-all": ""
            },
            "recursive": {
                "recursive": "-R",
                "no-recursive": ""
            },
            "dir": {
                "tmp": "/tmp",
                "root": "/root",
                "usrbin": "/usr/bin",
                "all": "/"
            }
        },
        "filters": [
            "(recursive != 'recursive') or (dir != 'tmp')"
        ],
        "storage": {
            "type": "file",
            "hash_dir": True
        }
    })


def test_ls() -> None:
    """Launch the test-ls benchmark and do various stuff on it."""
    benchmark = mk_test_ls_benchmark()
    benchmark.run()
    benchmark.show()
    for row in benchmark.extract():
        print(row)
    print(benchmark.stats())
    benchmark.delete()
