#!/usr/bin/env python3

"""Definition of class FileStorage."""

import typing as tp
import os
import datetime
import json
import hashlib
import shutil
from pathlib import Path

from . import Storage
if tp.TYPE_CHECKING:
    from . import Run, Benchmark


class FileStorage(Storage):
    """A class to store run results on the file system (1 directory / run)."""

    def __init__(self, bench: 'Benchmark', **kwargs: tp.Any):
        """Initialise self."""
        super().__init__(bench, **kwargs)
        self.hash_dir = kwargs.get('hash_dir', True)

    def _get_dir(self, run: 'Run') -> str:
        sub_dirs = list(run.assign.values())
        if self.hash_dir:
            hval = hashlib.md5(
                ';'.join(sub_dirs).encode('utf-8')
            ).hexdigest()
            result = os.path.join(hval[:2], hval[2:4], hval[4:])
        else:
            result = os.path.join(*sub_dirs)
        return os.path.join(self.bench.get_dir(), 'runs', result)

    def _get_mdata_file(self, run: 'Run') -> str:
        return os.path.join(self._get_dir(run), '_mdata.json')

    def _get_stderr(self, run: 'Run') -> str:
        return os.path.join(self._get_dir(run), '_stderr')

    def _get_stdout(self, run: 'Run') -> str:
        return os.path.join(self._get_dir(run), '_stdout')

    def _get_data_file(self, run: 'Run') -> str:
        return os.path.join(self._get_dir(run), '_data.zip')

    def store(self, run: 'Run') -> None:
        Path(self._get_dir(run)).mkdir(parents=True, exist_ok=True)
        with open(self._get_mdata_file(run), 'w', encoding='utf-8') as fd:
            data = {
                'command': run.get_full_cmd(),
                'assign': {
                    param.name: value
                    for param, value in run.assign.items()
                },
                'start_time': str(run.start_time),
                'end_time': str(run.end_time),
                'exit_code': run.exit_code,
                'exec_time': run.exec_time,
                'timeout': run.timeout
            }
            fd.write(json.dumps(data, indent=2))
        with open(self._get_stderr(run), 'w', encoding='utf-8') as fd:
            fd.write(run.stderr)
        with open(self._get_stdout(run), 'w', encoding='utf-8') as fd:
            fd.write(run.stdout)
        with open(self._get_data_file(run), 'wb') as bfd:
            bfd.write(run.data)

    def load(self, run: 'Run') -> None:
        try:
            with open(self._get_mdata_file(run), encoding='utf-8') as fd:
                result = json.loads(fd.read())
                parse_date = datetime.datetime.fromisoformat
                run.done = True
                run.exit_code = int(result['exit_code'])
                run.timeout = bool(result['timeout'])
                run.start_time = parse_date(result['start_time'])
                run.end_time = parse_date(result['end_time'])
                run.exec_time = float(result['exec_time'])
                with open(self._get_stderr(run), encoding='utf-8') as fd:
                    run.stderr = fd.read()
                with open(self._get_stdout(run), encoding='utf-8') as fd:
                    run.stdout = fd.read()
                with open(self._get_data_file(run), 'rb') as bfd:
                    run.data = bfd.read()
        except (json.decoder.JSONDecodeError, FileNotFoundError):
            run.done = False

    def delete(self, run: 'Run') -> bool:
        d = self._get_dir(run)
        result = os.path.exists(d)
        shutil.rmtree(d)
        return result

    def serialise(self) -> tp.Dict[str, tp.Any]:
        return {
            'type': 'file',
            'hash_dir': self.hash_dir
        }
