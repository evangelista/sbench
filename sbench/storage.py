#!/usr/bin/env python3

"""Definition of class Storage."""

import typing as tp

if tp.TYPE_CHECKING:
    from . import Run, Benchmark


class Storage:
    """An abstract class for objects that can be used to store run results."""

    def __init__(
            self,
            bench: 'Benchmark',
            **kwargs: tp.Any
    ):  # pylint: disable=unused-argument
        """Initialise self."""
        self.bench: 'Benchmark' = bench

    def setup(self) -> None:
        """Set everything up to initialise the storage (e.g. create a db)."""

    def store(self, run: 'Run') -> None:
        """Store a run result."""

    def load(self, run: 'Run') -> None:
        """Load a run result."""

    def delete(self, run: 'Run') -> bool:  # pylint: disable=unused-argument
        """Delete a run result."""
        return False

    def serialise(self) -> tp.Dict[str, tp.Any]:
        """Return a JSON serialisation of self."""
        return dict()
