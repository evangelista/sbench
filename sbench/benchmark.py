#!/usr/bin/env python3

"""Definition of class Benchmark."""

from __future__ import annotations
from pathlib import Path
import typing as tp
import os
import json
import random
import shutil
import pkg_resources

from . import LightDatabaseStorage, FileStorage, Storage, Param, Run
from . import filters, naming, types


class Benchmark:
    """A Benchmark consists of a set of runs to launch.

    The class provides a way to specify a set of runs to be launched.
    Each run is characterized by the base command of the benchmark
    followed by an assignment of some parameters of this command.

    """

    ROOT_DIR = os.path.join(Path.home(), ".local", "share", "sbench")

    def __init__(self, **kwargs: tp.Any):
        """Initialise a Benchmark object.

        kwarg name = name of the benchmark

        kwarg cmd = base command of the benchmark

        kwarg timeout = None or maximal execution time (in sec.) for a
        run

        kwarg params = parameters to be checked (see example below)

        kwarg filters = string list of filter expressions (see example
        below)

        kwarg storage = dictionary containing storage arguments

        >>> bench = Benchmark(
        ...    name="my-bench",
        ...    cmd="/path/to/my/executable",
        ...    timeout=5,
        ...    params={
        ...       "first-param": {
        ...          "10": "--some-arg 10",
        ...          "20": "--some-arg 20"
        ...       },
        ...       "second-param": {
        ...          "yes": "--another-arg",
        ...          "no": ""
        ...       },
        ...    },
        ...    filters=[
        ...       "first-param == '20' => second-param != 'no'"
        ...    ],
        ...    storage={
        ...       "type": "lightdb"
        ...    }
        ... )
        >>> bench
        <sbench.benchmark.Benchmark object at ...>

        >>> for run in bench.iter_runs():
        ...   print(run.get_full_cmd())
        /path/to/my/executable --some-arg 10 --another-arg
        /path/to/my/executable --some-arg 10
        /path/to/my/executable --some-arg 20 --another-arg

        """
        naming.check(kwargs["name"], naming.RE_BENCHMARK_NAME)

        self.name: str = kwargs["name"]
        self.cmd: str = kwargs["cmd"]
        self.timeout = kwargs.get("timeout")
        self.params = [
            Param(name, params)
            for name, params in kwargs.get("params", {}).items()
        ]
        self.filters = list(
            map(str, (flt for flt in kwargs.get("filters", [])))
        )
        storage = kwargs.get("storage", {"type": "lightdb"})
        self.storage: Storage = {
            "lightdb": LightDatabaseStorage,
            "file": FileStorage
        }[storage.get("type", "lightdb")](self, **storage)

    def get_dir(self) -> str:
        """Get the output directory in which benchmark data will be stored."""
        return os.path.join(Benchmark.ROOT_DIR, self.name)

    def get_properties_file(self) -> str:
        """Get the json file in which benchmark properties is stored."""
        return os.path.join(self.get_dir(), "sbench.json")

    def iter_runs(
            self,
            randomize: bool = False
    ) -> tp.Iterator[Run]:
        """Generate all the runs of the benchmark.

        If randomize is True, runs will be generated in a random
        order.

        """
        def evaluate_filters(run: None | Run) -> bool:
            for flt in self.filters:
                eval_result = filters.evaluate(flt, assignment, run=run)
                if eval_result is not None and not eval_result:
                    return False
            return True

        def loop(i: int) -> tp.Iterator[Run]:
            if i == len(params):
                run = Run(self, dict(assignment))
                if evaluate_filters(run):
                    yield run
            else:
                param = params[i]
                values = list(param.values.keys())
                if randomize:
                    random.shuffle(values)
                for value in values:
                    assignment[param] = value
                    if evaluate_filters(None):
                        yield from loop(i + 1)
                    del assignment[param]
        params = list(self.params)
        assignment: tp.Dict[Param, str] = dict()
        yield from loop(0)

    def run(
            self,
            randomize: bool = False,
            rerun_timeouts: bool = False,
            rerun_errors: bool = False
    ) -> int:
        """Launch all runs of the benchmark.

        Return the number of runs launched.  If randomize is True,
        launch order is randomized.  If rerun_timeouts
        (resp. rerun_errors) is True, runs that ended with a timeout
        (resp. an error) during a previous run are launched again.

        """
        self.save(self.get_properties_file())
        self.storage.setup()
        result = 0
        try:
            for r in self.iter_runs(randomize=randomize):
                launch = not r.done
                launch = launch or (rerun_timeouts and r.is_timeout())
                launch = launch or (
                    rerun_errors and not r.is_success() and not r.is_timeout()
                )
                if launch:
                    r.launch()
                    self.storage.store(r)
                    result += 1
        except KeyboardInterrupt:
            pass
        return result

    def stats(self) -> types.benchmark_stats_t:
        """Get a dictionary with general statistics on the benchmark."""
        result: types.benchmark_stats_t = {
            "name": self.name,
            "done": 0,
            "not_done": 0,
            "success": 0,
            "failure": 0,
            "timeout": 0
        }
        for run in self.iter_runs():
            if not run.done:
                result["not_done"] += 1
            else:
                result["done"] += 1
                if run.is_success():
                    result["success"] += 1
                else:
                    result["failure"] += 1
                if run.is_timeout():
                    result["timeout"] += 1
        return result

    def extract(
            self,
            to_csv: bool = True,
            html_dir: None | str = None,
            handler_cmd: None | str = None,
            handler_fun: None | tp.Callable[[Run], tp.Iterator[str]] = None,
            sep: str = ";",
            fmt: None | str = None
    ) -> tp.Iterator[str]:
        """Extract data on terminated runs of the benchmark."""
        enc = "utf-8"
        if html_dir is not None:
            os.makedirs(html_dir, exist_ok=True)
            data_dir = pkg_resources.resource_filename("sbench", "data")
            for f in ["styles.css", "script.js"]:
                shutil.copyfile(
                    os.path.join(data_dir, f),
                    os.path.join(html_dir, f)
                )

            # create html index file
            html_file = os.path.join(data_dir, "index.html")
            with open(html_file, encoding=enc) as fd:
                html = fd.read().format(
                    title=f"Results for benchmark {self.name}"
                )
            html_file = os.path.join(html_dir, "index.html")
            with open(html_file, "w", encoding=enc) as fd:
                fd.write(html)

            # create js data file
            js_file = os.path.join(html_dir, "data.js")
            js_fd = open(  # pylint: disable=consider-using-with
                js_file, "w", encoding=enc
            )
            json_data = json.dumps({
                param.name: list(param.values.keys())
                for param in self.params
            })
            js_fd.write(f"params = JSON.parse('{json_data}');\n")
            js_fd.write("var runs = [];\n")

        for run in self.iter_runs():
            if html_dir is not None:
                json_data = json.dumps(run.serialise())
                js_fd.write(f"runs.push(JSON.parse('{json_data}'));\n")
            if to_csv:
                yield from run.extract_csv(
                    cmd=handler_cmd,
                    fun=handler_fun,
                    sep=sep,
                    fmt=fmt
                )
        if html_dir is not None:
            js_fd.close()

    def delete(self) -> int:
        """Delete data of all runs of the benchmark.

        Return the number of runs of which data have been deleted.

        """
        return sum(1 for run in self.iter_runs() if self.storage.delete(run))

    def show(self) -> None:
        """Print information on each run."""
        print(79 * "=")
        for run in self.iter_runs():
            if run.done:
                run.show()
                print(79 * "=")

    def serialise(self) -> tp.Dict[str, tp.Any]:
        """Get a json-serialisable dictionary of benchmark properties."""
        return {
            "name": self.name,
            "cmd": self.cmd,
            "timeout": self.timeout,
            "params": {
                param.name: param.values
                for param in self.params
            },
            "filters": list(self.filters),
            "storage": self.storage.serialise()
        }

    def save(self, json_file: str) -> None:
        """Save the benchmark properties in json_file."""
        os.makedirs(self.get_dir(), exist_ok=True)
        with open(json_file, "w", encoding="utf-8") as fd:
            fd.write(json.dumps(self.serialise(), indent=2))

    @classmethod
    def load(cls, json_file: str) -> Benchmark:
        """Load the benchmark stored in json_file."""
        with open(json_file, "r", encoding="utf-8") as fd:
            return Benchmark(**json.loads(fd.read()))

    @classmethod
    def load_from_dir(cls, bench_dir: str) -> Benchmark:
        """Load the benchmark of which the output directory is bench_dir."""
        return Benchmark.load(os.path.join(bench_dir, "sbench.json"))
