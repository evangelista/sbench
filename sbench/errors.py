#!/usr/bin/env python3

"""Definition of some Exception classes."""


class FilterParseException(Exception):
    """Exception raised when a filter could not be parsed."""

    def __init__(self, filter_expr: str):
        """Initialise self.

        filter_expr = filter expression that could not be parsed.

        """
        super().__init__(self)
        self.filter_expr = filter_expr

    def __str__(self) -> str:
        return f'filter expression "{self.filter_expr}" could not be parsed'


class NamingException(Exception):
    """Exception raised when a name does not respect a naming convention."""

    def __init__(self, name: str, name_re: str):
        """Initialise self.

        name = provided name, name_re = regular expression the name
        should have matched.

        """
        super().__init__(self)
        self.name = name
        self.name_re = name_re

    def __str__(self) -> str:
        return f'"{self.name}" does not match re "{self.name_re}"'
