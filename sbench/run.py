#!/usr/bin/env python3

"""Definition of class Run."""

from __future__ import annotations
import typing as tp
from typing import TYPE_CHECKING
import os
import subprocess
import datetime
import tempfile
import shutil

from . import Param, io, types
if TYPE_CHECKING:
    from . import Benchmark


class Run:
    """A Run is an instanciation of the parameters of a Benchmark."""

    def __init__(
            self,
            benchmark: "Benchmark",
            assign: tp.Dict[Param, str]
    ):
        """Initialise self with the given benchmark and a parameter assignment.

        Dictionary assign maps Parameters to couple (name, arg) where
        name is the name associated to the value and arg is the
        argument that will be passed to the command.

        """
        self.benchmark = benchmark
        self.assign = assign
        self.done: bool = False
        self.exit_code: int
        self.timeout: bool
        self.start_time: datetime.datetime
        self.end_time: datetime.datetime
        self.exec_time: float
        self.stdout: str
        self.stderr: str
        self.tmp_dir: str = ""
        self.data: bytes
        benchmark.storage.load(self)

    def __del__(self) -> None:
        """Delete the temporary directory of the run."""
        if os.path.isdir(self.tmp_dir):
            shutil.rmtree(self.tmp_dir)

    def get_result(self) -> types.run_result_t:
        """Get the result of the run as a dictionary.

        The resulting dictionary is empty if the run has not been
        launched yet.

        """
        if not self.done:
            return dict()
        return {
            "command": self.get_full_cmd(),
            "assign": self.get_assign(),
            "start_time": self.start_time,
            "end_time": self.end_time,
            "exit_code": self.exit_code,
            "exec_time": self.exec_time,
            "timeout": self.timeout
        }

    def get_id(self) -> str:
        """Return a string that identifies self."""
        return ";".join([
            f"{param.name}={value}"
            for param, value in sorted(
                    self.assign.items(),
                    key=lambda param_value: param_value[0].name
            )
        ])

    def get_data_dir(self) -> str:
        """Return the directory path where data for the run are stored."""
        return os.path.join(self.tmp_dir, "data")

    def get_assign(self) -> tp.Dict[str, str]:
        """Get a dictionary associating parameter names to values for self."""
        return {param.name: value for param, value in self.assign.items()}

    def get_full_cmd(self) -> str:
        """Get the full command that will be launched for the run.

        The full command is obtained by concatenating the base command
        with parameter values.

        """
        result = self.benchmark.cmd
        for param, value in self.assign.items():
            arg = param.get_value_arg(value)
            if arg != "":
                result += " " + arg
        return result

    def is_success(self) -> bool:
        """Check if self has successfully completed.

        Return True if it has been launched and has terminated with an
        exit code != 0, False otherwise.

        """
        return self.done and self.exit_code == 0

    def is_timeout(self) -> bool:
        """Check if the run did timeout.

        Return True if the run has been launched and has not
        terminated within the benchmark timeout value.

        """
        return self.done and self.timeout

    def env_vars(self) -> tp.Dict[str, str]:
        """Get environment variables that are set before executing the run."""
        return {
            **os.environ,
            "SBENCH_DATA_DIR": self.get_data_dir()
        }

    def launch(self) -> None:
        """Launch the run by executing self.get_full_cmd().

        Result of the run are saved in the database.

        """
        full_cmd = self.get_full_cmd()
        io.msg(full_cmd)
        self.start_time = datetime.datetime.now()
        self.tmp_dir = tempfile.mkdtemp()
        data_dir = os.path.join(self.get_data_dir())
        os.mkdir(data_dir)
        stderr_file = os.path.join(self.tmp_dir, "stderr")
        stdout_file = os.path.join(self.tmp_dir, "stdout")

        #  launch the run
        with (
                open(stderr_file, "w", encoding="utf-8") as fd_err,
                open(stdout_file, "w", encoding="utf-8") as fd_out
        ):
            try:
                proc = subprocess.run(
                    full_cmd,
                    shell=True,
                    stdout=fd_out,
                    stderr=fd_err,
                    encoding="utf-8",
                    env=self.env_vars(),
                    timeout=self.benchmark.timeout,
                    check=False
                )
                self.timeout = False
                self.exit_code = proc.returncode
                io.msg(f"> done (exit code = {self.exit_code})!")
            except subprocess.TimeoutExpired:
                self.timeout = True
                self.exit_code = 128
                io.msg("> timeout!")
        self.done = True
        self.end_time = datetime.datetime.now()
        diff = self.end_time - self.start_time
        self.exec_time = diff.seconds + diff.microseconds / 1_000_000

        #  read stdout and stderr
        with open(stderr_file, "rb") as fd:
            self.stderr = str(fd.read())
        with open(stdout_file, "rb") as fd:
            self.stdout = str(fd.read())

        #  archive self's data directory
        zip_file = os.path.join(self.tmp_dir, "archive")
        shutil.make_archive(zip_file, "zip", data_dir)
        with open(zip_file + ".zip", "rb") as bfd:
            self.data = bytes(bfd.read())

    def extract_csv(
            self,
            cmd: None | str = None,
            fun: None | tp.Callable[[Run], tp.Iterator[str]] = None,
            sep: str = ";",
            fmt: None | str = None
    ) -> tp.Iterator[str]:
        """Generate a csv line for self containing run data.

        Values are separated with character ";".  Does not generate
        anything if the run has not completed yet.

        If cmd is not None, then it must contain a command that will
        be executed for this run and that must print to standard
        output a list of ";" separated values that are appended to run
        data.  self.env_vars() are set before calling the command.

        Similarly, if fun is not None, then it must yield the strings
        that are appended to run data for the run.

        sep is the field separator string.

        """
        if not self.done:
            return

        #  extract the run data
        self.tmp_dir = tempfile.mkdtemp()
        data_dir = os.path.join(self.get_data_dir())
        tmp_archive = tempfile.mktemp(suffix=".zip")
        with open(tmp_archive, "wb") as fd:
            fd.write(self.data)
        shutil.unpack_archive(tmp_archive, extract_dir=data_dir)
        os.remove(tmp_archive)

        # set first fields (run parameter, exit_code, ...)
        if fmt is None:
            row = sep.join([
                *self.assign.values(),
                str(self.exit_code),
                str(int(self.timeout)),
                str(self.exec_time)
            ])
        else:
            row = fmt.format(**{
                **{param.name: val for param, val in self.assign.items()},
                "exit_code": str(self.exit_code),
                "timeout": str(int(self.timeout)),
                "exec_time": str(self.exec_time)
            })

        # now call fun or cmd
        if fun is None and cmd is None:
            yield row
        elif fun is not None:
            for line in fun(self):
                yield row + sep + line
        else:
            assert cmd is not None
            with subprocess.Popen(
                    cmd,
                    shell=True,
                    stdout=subprocess.PIPE,
                    encoding="utf-8",
                    env=self.env_vars()
            ) as proc:
                if proc.stdout is not None:
                    loop = True
                    while loop:
                        line = proc.stdout.readline()
                        if line != "":
                            yield row + sep + line.split("\n")[0]
                        loop = line != "" or proc.poll() is None

    def serialise(self) -> tp.Dict[str, tp.Any]:
        """Get a json dumpable dictionary containing all the run data."""
        result: tp.Dict[str, tp.Any] = {
            "command": self.get_full_cmd(),
            "exit_code": None,
            "exec_time": None,
            "timeout": None,
            "stdout": None,
            "stderr": None,
            "start_time": None,
            "end_time": None
        }
        if self.done:
            result = {
                **result,
                **self.get_result()
            }
            for t in ("end_time", "start_time"):
                result[t] = datetime.datetime.isoformat(result[t])
        result["assign"] = {p.name: v for p, v in self.assign.items()}
        return result

    def show(self) -> None:
        """Print information of the run to stdout.

        Informations printed include the contents of stdout, stderr,
        and data.json.

        """
        print(">>>>>  data  <<<<<")
        for key, val in self.get_result().items():
            if key not in {"stdout", "stderr"}:
                print(key, "=", val)
        print(">>>>> stdout <<<<<")
        print(self.stdout)
        print(">>>>> stderr <<<<<")
        print(self.stderr)
