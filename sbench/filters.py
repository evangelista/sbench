#!/usr/bin/env python3

"""Definition of class Filter."""

from __future__ import annotations
import typing as tp

from .errors import FilterParseException
from . import Param, Run


def evaluate(
        filter_expr: str,
        assign: tp.Dict[Param, str],
        run: None | Run = None
) -> None | bool:
    """Evaluate filter_expr with parameter assignment assign.

    The optional run which may be used to check for, e.g., exit_code
    or timeout.

    Return True or False if the expression could have been evaluated
    with the given assignment.  Return None if filter_expr could not
    have been evaluated because it contains some parameter x for which
    x is not in assign.

    """
    glob: dict[str, tp.Any] = {
        param.name: value
        for param, value in assign.items()
    }
    if run is not None:
        run_result = run.get_result()
        for var in ["exit_code", "timeout"]:
            if var in run_result:
                glob[var] = run_result[
                    tp.cast(tp.Literal["exit_code", "timeout"], var)
                ]
    try:
        return bool(
            eval(  # pylint: disable=eval-used
                filter_expr,
                glob
            )
        )
    except NameError:
        return None
    except SyntaxError as exc:
        raise FilterParseException(filter_expr) from exc
