#!/usr/bin/env python3

"""This package defines some regular expressions for naming."""

import re

from .errors import NamingException


#  a benchmark name
RE_BENCHMARK_NAME = r'[a-zA-Z][\.a-zA-Z\-\_0-9]*'

#  a parameter name
RE_PARAM_NAME = r'[a-zA-Z][\.a-zA-Z\-\_0-9]*'

#  a parameter value
RE_PARAM_VALUE = r'[\.a-zA-Z\-\_0-9:]+'


def check(name: str, name_re: str) -> None:
    """Check that name matches name_re regular expression.

    Raise Errors.NamingException if the expression is not matched.

    """
    if not re.compile(name_re).fullmatch(name):
        raise NamingException(str(name), str(name_re))
