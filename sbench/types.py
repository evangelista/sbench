#!/usr/bin/env python3

"""Definition of some types for type checking."""

import typing as tp
import datetime

benchmark_stats_t = tp.TypedDict('benchmark_stats_t', {
    'name': str,
    'done': int,
    'not_done': int,
    'success': int,
    'failure': int,
    'timeout': int
})

run_result_t = tp.TypedDict('run_result_t', {
    'command': str,
    'assign': tp.Dict[str, str],
    'start_time': datetime.datetime,
    'end_time': datetime.datetime,
    'exit_code': tp.Optional[int],
    'exec_time': float,
    'timeout': bool
}, total=False)
