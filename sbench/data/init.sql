create table run (
   assign varchar primary key,
   command varchar not null,
   exit_code integer not null,
   timeout boolean not null,
   start_time varchar not null,
   end_time varchar not null,
   exec_time float not null,
   stdout blob not null,
   stderr blob not null,
   data blob
);
