html = '<table>';
var select = '<select id="status">';
select += '<option value="*">*</option>'
select += '<option value="error">error</option>'
select += '<option value="timeout">timeout</option>'
select += '<option value="ok">ok</option>'
select += '<option value="unknown">unknown</option>'
select += '</select>';
html += '<tr><td>status</td><td>' + select + '</td></tr>';
for(var param in params) {
    var values = params[param];
    var select = '<select id="param-' + param + '">';
    select += '<option value="*">*</option>';
    for(var value in values) {
        var value = values[value];
        select += '<option value="' + value + '">' + value + '</option>';
    }
    select += '</select>';
    html += '<tr><td>' + param + '</td><td>' + select + '</td></tr>';
}
html += '<tr><td colspan="2" style="text-align:center;">';
html += '<a href="javascript:validate()">'
html += 'show</a></td></tr>';
html += '</table>';
html += '<span id="results-num"></span>'

$('#filters').html(html);
$('#results-num').hide();

var MAX_FOUND = 10000;

function fix_null(val) {
    if(val == null) {
        return 'NA';
    }
    return val;
}

function validate() {
    var filters = {};
    var html = '';
    $("#tbl-results").find("tr:gt(0)").remove();
    $("#results").html("");
    for(var param in params) {
        var val = $('#param-' + param).val();
        filters[param] = val;
    }
    filters['status'] = $('#status').val();
    var found = 0;
    for(var i = 0; i < runs.length; i ++) {
        var run = runs[i];
        var assign = run['assign'];
        var ok = true;
        var status = '';
        for(var param in assign) {
            if(assign[param] != filters[param] && filters[param] != '*') {
                ok = false;
            }
        }
        if(!ok) {
            continue;
        }
        exit_code = run['exit_code'];
        timeout = run['timeout'];
        command = run['command'];
        exec_time = fix_null(run['exec_time']);
        stdout = fix_null(run['stdout']);
        stderr = fix_null(run['stderr']);
        if(timeout != null && timeout) {
            status = 'timeout';
        } else {
            if(exit_code == null) {
                status = 'unknown';
            } else if(exit_code == 0) {
                status = 'ok';
            } else {
                status = 'error';
            }
        }
        if(filters['status'] != null
           && filters['status'] != '*'
           && filters['status'] != status) {
            continue;
        }
        if(exec_time != 'NA') {
            exec_time = exec_time.toFixed(2);
        }
        if(stdout != 'NA') {
            stdout = '<a href="' + stdout + '">stdout</a>';
        }
        if(stderr != 'NA') {
            stderr = '<a href="' + stderr + '">stderr</a>';
        }
        html += '<tr><td><ul>';
        for(var param in params) {
            html += '<li>' + param + ' = ' +  assign[param] + '</li>';
        }
        html += '</ul></td>';
        html += '<td class="command">' + command + '</td>';
        html += '<td class="run-' + status + '">';
        html += status.toUpperCase() + '</td>';
        html += '<td class="exec-time">' + exec_time + ' s.</td>';
        html += '<td>' + stdout + ' / ' + stderr + '</td>';
        html += '</td>';
        html += '</tr>\n';
        found ++;
        if(found >= MAX_FOUND) {
            alert('Too much runs reported (>= ' + MAX_FOUND + ')!');
            return;
        }
    }
    if(found > 0) {
        html = `
    <table id="tbl-results">
      <tr>
        <th>Parameters</td>
        <th>Command</td>
        <th>Status</td>
        <th>Time</td>
        <th>Output files</td>
      </tr>` + html + `
        </table>`;
    }
    $("#results").html(html);

    $('#results-num')
        .html(found + ' result(s) found')
        .show();
}
