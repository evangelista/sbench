#!/usr/bin/env python3

"""Definition of class Param."""

from typing import Dict

from . import naming


class Param:
    """A Param object corresponds to a benchmark parameter."""

    def __init__(
            self,
            name: str,
            values: Dict[str, str]
    ):
        """Initialise self with a name and a possible values.

        The values dictionary maps value-names to arguments (possibly
        the empty string) that will passed to the command.

        >>> Param('reverse', {'yes': 'r', 'no': ''})
        <sbench.param.Param object at ...>

        """
        naming.check(name, naming.RE_PARAM_NAME)
        for value in values:
            naming.check(value, naming.RE_PARAM_VALUE)
        self.name = name
        self.values = values

    def get_value_arg(self, value: str) -> str:
        """Get the command line argument associated to value."""
        return self.values[value]
