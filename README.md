# sbench


## Description

sbench is a simple benchmarking tool written in python


## License

sbench is published under the term of
[GPLv3](https://www.gnu.org/licenses/gpl-3.0.txt)


## Installation

install sbench with the `setup.py` script

```
python setup.py install --user
```

## Usage
```
usage: sbench [-h] [-a {run,stats,extract,show,delete}] [-n HANDLER] [-r] [-v]
              [-V] [-f FILTER] [-t] [-e] [--force] [--html HTML_DIR]
			  [--separator SEPARATOR] [--format-string FORMAT_STRING]
              json
```

### Positional arguments
* `json`
  json file of the benchmark to process

### Optional arguments
* `-h`, `--help`
  show this help message and exit
* `-a ACTION`, `--action ACTION`
  set action to perform
  * choices: `run`, `stats`, `extract`, `show`, `delete`
  * default: `run`
* `-n HANDLER`, `--handler HANDLER`
  (with option `-a extract`) set the command that will be used to extract each test to CSV data
* `-r`, `--random`
  (with option `-a run`) randomize tests
* `-v`, `--verbose`
  be verbose
* `-V`, `--version`
  print the version number and exit
* `-f FILTER`, `--filter FILTER`
  add a filter expression
* `-t`, `--rerun-timeouts`
  (with option `-a run`) rerun tests that ended with a timeout during a previous execution of sbench
* `-e`, `--rerun-errors`
  (with option `-a run`) rerun tests that ended with an error during a previous execution of sbench
* `--force`
  (with option `-a delete`) do not prompt the user and force the deletion of test data
* `--html HTML_DIR`
  (with option `-a extract`) extract data in HTML format in directory html
* `--separator`
  (with option `-a extract`) set the field separator in output csv
* `--html HTML_DIR`
  (with option `-a extract`) format string in output csv
